# IF214005 - Praktikum Pemrograman Berorientasi Objek

## Learning Outcome Outline
- Basic Object Oriented Programming (OOP)
    - Class
    - Object
    - Attribute
    - Method
- Attribute
    - Data structure
- Method
    - Parameter
    - Return
    - Overloading
    - Overriding
    - Control
    - Loop
- Inheritance
- Encapsulation
- Abstraction
- Polymorphism
- Association
- Aggregation
- Composition
- Implementation

## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
1 | <ul><li>Mampu mendemonstrasikan dasar pemrograman berorientasi objek</li></ul> | <ul><li>[☕ Modul 1](./Modul 1)</li></ul>
2 | Mampu mendemonstrasikan penggunaan<br /><ul><li>Class & Object instantiation</li><li>Attribute (modifier, data type, parameter)</li><li>Method (modifier, data type, parameter)</li></ul> | [☕ materi](./class)
3 | |
4 | |
5 | |
6 | |
7 | |
 | UTS |
8 | |
9 | |
10 | |
12 | |
13 | |
14 | |
 | UAS |

## Tools
- [JDoodle](https://www.jdoodle.com/online-java-compiler/)

## Referensi
- [Orient Software - List of Object Oriented Programming Languages](https://www.orientsoftware.com/blog/list-of-object-oriented-programming-languages/)

### Materi
- 

### Kurikulum
- [KAIST - EE474 Introduction to Multimedia](https://sites.google.com/site/kaistee474itm/syllabus)
